package Entidad.Utilidad;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Fechas {
    
    public static String getFecha() {
        GregorianCalendar calendar = new GregorianCalendar();//Crea un objeto que representa el instante actual
        String year=""+calendar.get(Calendar.YEAR);
        String mes=""+(calendar.get(Calendar.MONTH)+1);
        String dia=""+calendar.get(Calendar.DAY_OF_MONTH);
        if (mes.length()==1) mes="0"+mes;        
        if (dia.length()==1) dia="0"+dia;
       return year+"-"+mes+"-"+dia;           
    }
    public static String getHora() {
        GregorianCalendar calendar = new GregorianCalendar();//Crea un objeto que representa el instante actual
        String hora=""+calendar.get(Calendar.HOUR_OF_DAY);
        String minuto=""+calendar.get(Calendar.MINUTE);
        if (hora.length()==1) hora="0"+hora;
        if (minuto.length()==1) minuto="0"+minuto;
        return hora+":"+minuto;           
    } 
    public static String getFechaHora() {
        String fechaActual=getFecha();
        String horaActual=getHora();
        return fechaActual+horaActual;           
    } 
    public int getFechatoInt()  {
        GregorianCalendar calendar = new GregorianCalendar();//Crea la fecha del dia actual
        String year=""+calendar.get(Calendar.YEAR);
        String mes=""+(calendar.get(Calendar.MONTH)+1);
        if (mes.length()==1) mes="0"+mes;
        String dia=""+calendar.get(Calendar.DAY_OF_MONTH);
        if (dia.length()==1) dia="0"+dia;
        int fecha=Integer.parseInt(year+mes+dia);
        return fecha;
    }
     public static String getFechaEsp(String fecha) {
         //fecha"2020-05-25"    25-05-2020
        return fecha.substring(8)+"-"+fecha.substring(5,7)+"-"+fecha.substring(0,4);
    } 
}
