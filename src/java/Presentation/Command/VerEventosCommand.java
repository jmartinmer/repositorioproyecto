/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.EventoBLL;
import Entidad.Evento;
import Entidad.Socio;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utilizan para ver TODOS los eventos
 * @author juan
 */
public class VerEventosCommand extends ICommand{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        EventoBLL eventoBLL= new EventoBLL();
        List<Evento> listaEventos= eventoBLL.obtenerEventos();
         if (listaEventos==null ||listaEventos.size()==0){
            request.setAttribute("mensaje", "Sin eventos");
            return "/mensaje.jsp";
        }
        request.setAttribute("ListaEventos", listaEventos);
        Socio socioSesion=(Socio)request.getSession().getAttribute("SocioSesion");
        List<Evento> listaEventosParticipa= eventoBLL.obtenerEventosParticipa(socioSesion);
        socioSesion.setEventosParticipa(listaEventosParticipa);
        request.getSession().setAttribute("SocioSesion",socioSesion);
        return "/verEventos.jsp";
    }
    
}
