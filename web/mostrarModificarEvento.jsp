
<%@page import="Entidad.TipoEvento"%>
<%@page import="Entidad.Evento"%>
<%  TipoEvento[] tipos = TipoEvento.values();
    Evento eventoSeleccionado = (Evento) request.getSession().getAttribute("EventoSeleccionado");
    int codigo = eventoSeleccionado.getCodigo();
    String titulo = eventoSeleccionado.getTitulo();
    String descripcion = eventoSeleccionado.getDescripcion();
    String tipo = eventoSeleccionado.getTipo().toString();
    String fecha = eventoSeleccionado.getFecha();
    String hora = eventoSeleccionado.getHora();
    String lugar = eventoSeleccionado.getLugar();
    int nsocio = eventoSeleccionado.getNsocio();
%>

<h1>Modificar evento</h1>
<form name ="form" action="Controller?opID=ModificarEvento" method="POST">
    <p><label>T�tulo</label><input type="text"  name="titulo" maxlength="20" required value="<%=titulo%>"></p>
    <p><label>Tipo</label><select name="tipo">
            <%
                for (int i = 0; i < tipos.length; i++) {
                    String valor = tipos[i].toString();
                    boolean seleccionado = valor.equals(tipo);
                    if (seleccionado) {
            %>
            <option selected value=<%=valor%>><%=valor%></option>
            <%} else {%>
            <option value=<%=valor%>><%=valor%></option>
            <%}
                }
            %>

        </select></p>
    <p><label>Fecha</label><input type="date"  name="fecha"  required value="<%=fecha%>"></p>
    <p><label>Hora</label><input type="time"  name="hora" required value="<%=hora%>"></p>
    <p><label>Lugar</label><input type="text"  name="lugar" required maxlength="20" value="<%=lugar%>"></p>
    <p><label>Descripcion</label><textarea name="descripcion" required rows="10"><%=descripcion%></textarea></p>
    <p><button>Enviar</button></p>
</form>
