/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *Un elemento de este enum representa uno de los posibles tipos de un evento
 * @author juan
 */
public enum TipoEvento {
    DESPLAZAMIENTO,ENTREGAPACKSOCIO,FIESTA,PREVIA,SOLIDARIO,VERPARTIDO;
}
