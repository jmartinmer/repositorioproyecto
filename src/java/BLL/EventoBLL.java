/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAO.Conexion_DB;
import DAO.EventoDAO;
import Entidad.Evento;
import Entidad.Exceptions.ProgException;
import Entidad.Socio;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juan
 */
public class EventoBLL {

    public List<Evento> obtenerEventos() throws Exception {
        Connection _con = null;
        List<Evento> _listado = new ArrayList();
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();// Abrimos la conexión
        EventoDAO _eventoDAO = new EventoDAO();

        _listado = _eventoDAO.obtenerEventos(_con);//Recuperamos los eventos

        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 

        return _listado;
    }
    
     public List<Socio> obtenerParticipantes(Evento evento) throws Exception {
        Connection _con = null;
        List<Socio> _listado = new ArrayList();
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();// Abrimos la conexión
        EventoDAO _eventoDAO = new EventoDAO();

        _listado = _eventoDAO.obtenerParticipantes(_con, evento);//Recuperamos los socios participantes de un evento

        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 

        return _listado;
    }

    public void crearEvento(Evento evento) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        EventoDAO eventoDAO = new EventoDAO();
        if (eventoDAO.findByCod(_con, evento) != null) {
            throw new ProgException("No se puede registrar porque el evento ya existe");
        } else {
            eventoDAO.crearEvento(_con, evento);
        }
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 
    }

    public List<Evento> obtenerEventosParticipa(Socio socioSesion) throws Exception {
        Connection _con = null;
        List<Evento> _listado = new ArrayList();
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();// Abrimos la conexión
        EventoDAO _eventoDAO = new EventoDAO();

        _listado = _eventoDAO.obtenerEventosParticipa(_con, socioSesion);//Recuperamos los socios participantes de un evento

        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 

        return _listado;
    }
    
    public void participarEvento(Evento evento, Socio socio) throws Exception{
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        EventoDAO eventoDAO = new EventoDAO();
        eventoDAO.participarEvento(_con, evento, socio);
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 
    }
    
    public void abandonarEvento(Evento evento, Socio socio) throws Exception{
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        EventoDAO eventoDAO = new EventoDAO();
        eventoDAO.abandonarEvento(_con, evento, socio);
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 
    }

    public List<Evento> obtenerEventosPropuestos(Socio socioSesion) throws Exception {
        Connection _con = null;
        List<Evento> _listado = new ArrayList();
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();// Abrimos la conexión
        EventoDAO _eventoDAO = new EventoDAO();

        _listado = _eventoDAO.obtenerEventosPropuestos(_con, socioSesion);//Recuperamos los eventos propuestos por un socio

        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 

        return _listado;
    }
    
    public Evento findByCod(Evento evento) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        EventoDAO eventoDAO = new EventoDAO();
        evento=eventoDAO.findByCod(_con, evento);
         _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 
        return evento;
    }

    public void modificarEvento(Evento evento) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        EventoDAO eventoDAO = new EventoDAO();
        if (eventoDAO.findByCod(_con, evento) == null) {
            throw new ProgException("No se puede modificar porque el evento no existe");
        } else {
            eventoDAO.modificarEvento(_con, evento);
        }
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */  
    }

    public void eliminarEvento(Evento evento) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        EventoDAO eventoDAO = new EventoDAO();
        if (eventoDAO.findByCod(_con, evento) == null) {
            throw new ProgException("No se puede eliminar porque el evento no existe");
        } else {
            eventoDAO.eliminarEvento(_con, evento);
        }
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */
    }
    
    
}
