/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.EventoBLL;
import Entidad.Evento;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utlilizan para mostrar la información del evento a modificar
 * @author juan
 */
public class MostrarModificarEventoCommand extends ICommand{
/**
 * En este método se da valor al atributo de sesión "EventoSeleccionado" para almacenar 
 * el evento seleccionado por el usuario 
 * @param request
 * @param response
 * @return
 * @throws Exception 
 */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int codigoEvento= Integer.parseInt(request.getParameter("codigoEvento"));
        Evento evento= new Evento();
        evento.setCodigo(codigoEvento);
        EventoBLL eventoBLL= new EventoBLL();
        Evento eventoSeleccionado=eventoBLL.findByCod(evento);
        request.getSession().setAttribute("EventoSeleccionado", eventoSeleccionado);
        return "/mostrarModificarEvento.jsp";
    }
    
}
