/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAO.Conexion_DB;
import DAO.EventoDAO;
import DAO.SocioDAO;
import Entidad.Evento;
import Entidad.Exceptions.ProgException;
import Entidad.Socio;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juan
 */
public class SocioBLL {

    public void altaSocio(Socio socio) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        SocioDAO socioDAO = new SocioDAO();
        if (socioDAO.findByDNI(_con, socio) != null) {
            throw new ProgException("No se puede registrar porque el socio ya existe");
        } else {
            socioDAO.altaSocio(_con, socio);
        }
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 
    }

    public Socio validaSocio(Socio _socio) throws Exception {
        Connection _con = null;
        Socio _socioObtenido = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();// Abrimos la conexión
        SocioDAO _socioDAO = new SocioDAO();
        _socioObtenido = _socioDAO.findByDNI(_con, _socio);//Recuperamos el socio     
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */         
        if ((_socioObtenido == null)
                || (!_socioObtenido.getPassword().equals(_socio.getPassword()))) {            
            throw new ProgException("Usuario o clave incorrectas");
        }
        return _socioObtenido;
    }

    public void editarSocio(Socio socio) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        SocioDAO socioDAO = new SocioDAO();
        if (socioDAO.findByNsocio(_con, socio) == null) {
            throw new ProgException("No se puede editar porque el socio no existe");
        } else {
            socioDAO.editarSocio(_con, socio);
        }
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */  
    }

    public List<Socio> obtenerSocios(Socio socio) throws Exception {
        Connection _con = null;
        List<Socio> _listado = new ArrayList();
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();// Abrimos la conexión
        SocioDAO _socioDAO = new SocioDAO();

        _listado = _socioDAO.obtenerSocios(_con,socio);//Recuperamos todos los socios

        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 

        return _listado;
    }

    public Socio findByNsocio(Socio socio) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        SocioDAO socioDAO = new SocioDAO();
        socio=socioDAO.findByNsocio(_con, socio);
         _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */ 
        return socio;
    }

    public void eliminarSocio(Socio socio) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        SocioDAO socioDAO = new SocioDAO();
        if (socioDAO.findByNsocio(_con, socio) == null) {
            throw new ProgException("No se puede eliminar porque el socio no existe");
        } else {
            socioDAO.eliminarSocio(_con, socio);
        }
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */
    }
    
    public void cambiarAdministrador(Socio socio) throws Exception {
        Connection _con = null;
        Conexion_DB _conexion_DB = new Conexion_DB();
        _con = _conexion_DB.AbrirConexion();
        SocioDAO socioDAO = new SocioDAO();
        if (socioDAO.findByNsocio(_con, socio) == null) {
            throw new ProgException("No se puede cambiar porque el socio no existe");
        } else {
            socioDAO.cambiarAdministrador(_con, socio);
        }
        _conexion_DB.CerrarConexion(_con); //Cerramos la conexión */  
    }
}
