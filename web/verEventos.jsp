<%@page import="Entidad.Utilidad.Fechas"%>
<%@page import="Entidad.Socio"%>
<%@page import="java.util.List"%>
<%@page import="Entidad.Evento"%>
<% List<Evento> listaEventos = (List<Evento>) request.getAttribute("ListaEventos");
    Socio socioSesion = (Socio) request.getSession().getAttribute("SocioSesion");
    List<Evento> listaEventosParticipa = socioSesion.getEventosParticipa();
%>
<h1>Todos los eventos</h1>
<ol>
    <%
        for (Evento evento : listaEventos) {
            int codigo = evento.getCodigo();
            String titulo = evento.getTitulo();
            String descripcion = evento.getDescripcion();
            String tipo = evento.getTipo().toString();
            String fecha = Fechas.getFechaEsp(evento.getFecha());
            String hora = evento.getHora();
            String lugar = evento.getLugar();
            int nsocio = evento.getNsocio();
    %>
    <li>
        <span>C�digo:</span><%=codigo%>
        <span>Socio:</span><%=nsocio%>
        <span>T�tulo:</span><%=titulo%>
        <span>Tipo:</span><%=tipo%>
        <span>Fecha:</span><%=fecha%>
        <span>Hora:</span><%=hora%>
        <span>Lugar:</span><%=lugar%><br>
        <span>Descripci�n:</span><br><%=descripcion%>
        <p class="lados">
            <%
                String fechaHoraActual = Fechas.getFechaHora();
                /*
                 Si las dos cadenas son iguales el m�todo devuelve 0
                 Si la cadena que invoca al m�todo es menor, devuelve un n�mero negativo
                 Si la cadena que invoca al m�todo es mayor , devuelve un n�mero positivo
                 */
                if (fechaHoraActual.compareTo(fecha+hora) <= 0) {

                    //Comprobar si evento est� en listaEventosParticipa
                    boolean participa = false;
                    for (Evento eventoParticipa : listaEventosParticipa) {
                        if (evento.getCodigo() == eventoParticipa.getCodigo()) {
                            participa = true;
                            break;
                        }
                    }
                    if (participa) {
                     %>
            <a href="Controller?opID=AbandonarEvento&codigoEvento=<%=codigo%>">Dejar de participar</a>
                    <%
                    } else {
                    %>
            <a href="Controller?opID=ParticiparEvento&codigoEvento=<%=codigo%>">Participar</a> 
                    <%
                        }
                    %>
            <%
                }else{%>
                <span></span>
                <%}%>
            <a href="Controller?opID=MostrarParticipantes&codigoEvento=<%=codigo%>">Participantes</a>    
            <% }%>  
        </p>
    </li>    

</ol>
