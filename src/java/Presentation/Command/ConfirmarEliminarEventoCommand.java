/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.EventoBLL;
import Entidad.Evento;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase permiten al usuario confirmar la eliminación de un evento
 * @author juan
 */
public class ConfirmarEliminarEventoCommand extends ICommand{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int codigoEvento=Integer.parseInt(request.getParameter("codigoEvento"));
        Evento eventoSeleccionado=new Evento();
        eventoSeleccionado.setCodigo(codigoEvento);
        EventoBLL eventoBLL= new EventoBLL();
        eventoSeleccionado= eventoBLL.findByCod(eventoSeleccionado);
         request.getSession().setAttribute("EventoSeleccionado", eventoSeleccionado);
        return "/confirmarEliminarEvento.jsp";
    }
    
}
