<%@page import="Entidad.Socio"%>
<%@page import="Entidad.Utilidad.Fechas"%>
<%@page import="Entidad.TipoEvento"%>
<%@page import="java.util.List"%>
<%
    TipoEvento[] tipos = TipoEvento.values();
    String fechaActual = Fechas.getFecha();
    Socio _socioSesion = (Socio) request.getSession().getAttribute("SocioSesion");
    String nsocio = Integer.toString(_socioSesion.getNsocio());
%>

<h1>Crear nuevo evento</h1>
<form name ="form" action="Controller?opID=CrearEvento" method="POST">
    <p><label>T�tulo</label><input type="text"  name="titulo" maxlength="20" required></p>
    <p><label>Tipo</label><select name="tipo">
        <%
            for (int i = 0; i < tipos.length; i++) {
                String valor = tipos[i].toString();
        %>
        <option value=<%=valor%>><%=valor%></option>
        <%}
        %>

    </select></p>
    <p><label>Fecha del evento</label><input type="date"  name="fecha" min =<%=fechaActual%> required></p>
    <p><label>Hora</label><input type="time"  name="hora" required></p>
    <p><label>Lugar</label><input type="text"  name="lugar" maxlength="20" required></p>
    <p><label>Descripci�n</label><textarea name="descripcion" required rows="10"></textarea></p>
    <input type="text"  name="nsocio" hidden value=<%=nsocio%>></p>
    <p><button>Crear</button></p>
</form>