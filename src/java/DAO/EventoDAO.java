/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entidad.Evento;
import Entidad.Socio;
import Entidad.TipoEvento;
import Entidad.Utilidad.Log;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Los objetos de esta clase sirven para hacer consultas sobre eventos a la base de datos 
 * @author juan
 */
public class EventoDAO {

    /**
     * Este método da valores adecuados a los campos del objeto evento que
     * recibe como parámetro a partir de una fila del resultSet rs
     *
     * @param rs
     * @param evento
     * @throws SQLException
     */
    private void obtenEventoFila(ResultSet rs, Evento evento) throws SQLException {
        evento.setCodigo(rs.getInt("codigo"));
        evento.setDescripcion(rs.getString("descripcion"));
        evento.setFecha(rs.getString("fecha"));
        evento.setHora(rs.getString("hora"));
        evento.setLugar(rs.getString("lugar"));
        evento.setNsocio(rs.getInt("nsocio"));
        //el campo tipo de la clase Evento es un objeto TipoEvento
        evento.setTipo(TipoEvento.valueOf(rs.getString("tipo")));
        evento.setTitulo(rs.getString("titulo"));
    }

    /**
     * Este método devuelve la lista de TODOS los eventos
     *
     * @param con
     * @return La lista de los eventos 
     * @throws Exception Se produce la excepción cuando no se pueden consultar los eventos
     */
    public List<Evento> obtenerEventos(Connection con) throws Exception {
        List<Evento> _listaEventos = new ArrayList();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM eventos");
            rs = stmt.executeQuery();
            Evento _evento = null;
            while (rs.next()) {
                _evento = new Evento();
                obtenEventoFila(rs, _evento);
                _listaEventos.add(_evento);
            }
        } catch (SQLException ex) {
            Log.getInstance().error(ex);
            throw new Exception("Ha habido un problema al buscar los eventos " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close(); //Cerramos el resulset
            }
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
        return _listaEventos;
    }

   
/**
 * Consulta en la base de datos para obtener el registro del evento cuyo código coincide
 * con el que recibe el método como parámetro y da valor a los campos de dicho evento
 * @param con
 * @param evento Sólo contiene valor el campo del código del evento
 * @param return Devuelve el evento con todos los campos rellenos
 * @throws Exception Se produce la excepción cuando no se consigue hacer la consulta
 */
    public Evento findByCod(Connection con, Evento evento) throws Exception {
        Evento _evento = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM eventos WHERE codigo=?");
            stmt.setInt(1, evento.getCodigo());
            rs = stmt.executeQuery();
            while (rs.next()) {
                _evento = new Evento();
                obtenEventoFila(rs, _evento);
            }
        } catch (SQLException ex) {
            Log.getInstance().error(ex);
            throw new Exception("Ha habido un problema al buscar el evento por código " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close(); //Cerramos el resulset
            }
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
        return _evento;
    }
/**
 * Inserta un nuevo registro en la tabla de eventos
 * @param con
 * @param evento El objeto evento que contiene la información del nuevo registro a insertar
 * @throws Exception La excepción que se produce si no se consigue insertar nuevo registro
 */
    public void crearEvento(Connection con, Evento evento) throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("INSERT INTO eventos(titulo,descripcion,"
                    + "tipo,fecha,hora,lugar,nsocio) VALUES(?,?,?,?,?,?,?)");
            stmt.setString(1, evento.getTitulo());
            stmt.setString(2, evento.getDescripcion());
            stmt.setString(3, evento.getTipo().toString());
            stmt.setString(4, evento.getFecha());
            stmt.setString(5, evento.getHora());
            stmt.setString(6, evento.getLugar());
            stmt.setInt(7, evento.getNsocio());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Ha habido un problema al registrar el evento " + ex.getMessage());

        } finally {
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
    }
/**
 * Devuelve la lista de los participantes al evento que recibe como parámetro
 * @param con
 * @param evento
 * @return
 * @throws Exception 
 */
    public List<Socio> obtenerParticipantes(Connection con, Evento evento) throws Exception {
        List<Socio> _listaParticipantes = new ArrayList();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM socios , participantes WHERE socios.nsocio= participantes.nsocio AND codevento=?");
            stmt.setInt(1, evento.getCodigo());
            rs = stmt.executeQuery();
            Socio _socio = null;
            while (rs.next()) {
                _socio = new Socio();
                SocioDAO.obtenSocioFila(rs, _socio);
                _listaParticipantes.add(_socio);
            }
        } catch (SQLException ex) {
            Log.getInstance().error(ex);
            throw new Exception("Ha habido un problema al buscar los participantes " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close(); //Cerramos el resulset
            }
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
        return _listaParticipantes;
    }
/**
 * Devuelve la lista de los eventos en los que participa el socio que recibe como parámetro
 * @param con 
 * @param socioSesion
 * @return
 * @throws Exception 
 */
    public List<Evento> obtenerEventosParticipa(Connection con, Socio socioSesion) throws Exception {
        List<Evento> _listaEventosParticipa = new ArrayList();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT codigo, descripcion, fecha, hora, lugar, eventos.nsocio, tipo, titulo "
                    + "FROM eventos, participantes,socios WHERE eventos.codigo=participantes.codevento AND participantes.nsocio=socios.nsocio AND socios.dni=?");
            stmt.setString(1, socioSesion.getDni());
            rs = stmt.executeQuery();
            Evento evento = null;
            while (rs.next()) {
                evento = new Evento();
                obtenEventoFila(rs, evento);
                _listaEventosParticipa.add(evento);
            }
        } catch (SQLException ex) {
            Log.getInstance().error(ex);
            throw new Exception("Ha habido un problema al buscar los eventos en los que participa el socio " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close(); //Cerramos el resulset
            }
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
        return _listaEventosParticipa;
    }
/**
 * Inserta un nuevo registro en la tabla participantes 
 * @param con
 * @param evento El código de evento del registro a insertar
 * @param socio El número de socio del registro a insertar
 * @throws Exception 
 */
    public void participarEvento(Connection con, Evento evento, Socio socio) throws Exception{
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("INSERT INTO participantes(codevento, nsocio) VALUES (?,?)");
            stmt.setInt(1, evento.getCodigo());
            stmt.setInt(2, socio.getNsocio());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Ha habido un problema al registrar el participante " + ex.getMessage());

        } finally {
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
    }
/**
 * Elimina un registro de la tabla de participantes
 * @param con
 * @param evento El evento correspondiente al registro a eliminar
 * @param socio El socio correspondiente al registro a eliminar
 * @throws Exception 
 */
    public void abandonarEvento(Connection con, Evento evento, Socio socio) throws Exception{
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM participantes WHERE codevento=? AND nsocio=?");
            stmt.setInt(1, evento.getCodigo());
            stmt.setInt(2, socio.getNsocio());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Ha habido un problema al abandonar el evento" + ex.getMessage());

        } finally {
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
    }
/**
 * Devuelve la lista de los eventos propuestos por el socio que recibe como parámetro
 * @param con
 * @param socioSesion
 * @return
 * @throws Exception 
 */
    public List<Evento> obtenerEventosPropuestos(Connection con, Socio socioSesion) throws Exception {
        List<Evento> _listaEventosPropuestos = new ArrayList();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM eventos WHERE nsocio=?");
            stmt.setInt(1, socioSesion.getNsocio());
            rs = stmt.executeQuery();
            Evento evento = null;
            while (rs.next()) {
                evento = new Evento();
                obtenEventoFila(rs, evento);
                _listaEventosPropuestos.add(evento);
            }
        } catch (SQLException ex) {
            Log.getInstance().error(ex);
            throw new Exception("Ha habido un problema al buscar los eventos propuestos " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close(); //Cerramos el resulset
            }
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
        return _listaEventosPropuestos;
    }
/**
 * Modifica la información de un evento de la tabla de eventos
 * @param con
 * @param evento El objeto evento que contiene la información modificada
 * @throws Exception 
 */
    public void modificarEvento(Connection con, Evento evento) throws Exception {
        PreparedStatement stmt=null; 
        try {
            stmt = con.prepareStatement("UPDATE eventos SET titulo=?,descripcion=?,tipo=?,fecha=?,hora=?,lugar=? WHERE codigo=?");
            stmt.setString(1,evento.getTitulo());
            stmt.setString(2,evento.getDescripcion());
            stmt.setString(3,evento.getTipo().toString());
            stmt.setString(4,evento.getFecha());
            stmt.setString(5,evento.getHora());
            stmt.setString(6,evento.getLugar());
            stmt.setInt(7,evento.getCodigo());
            stmt.executeUpdate();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Ha habido un problema al modificar el evento "+ex.getMessage());
        }  finally
        {
            if (stmt != null) stmt.close();//Cerramos el Statement 
        } 
    }
/**
 * Elimina de la tabla eventos el evento cuyo código coincide con el del objeto Evento que recibe como parámetro
 * @param con
 * @param evento
 * @throws Exception 
 */
    public void eliminarEvento(Connection con, Evento evento) throws Exception {
        PreparedStatement stmt=null; 
        try {
            stmt = con.prepareStatement("DELETE FROM eventos WHERE codigo=?");
            stmt.setInt(1,evento.getCodigo());
            stmt.executeUpdate();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Ha habido un problema al eliminar el evento "+ex.getMessage());
        }  finally
        {
            if (stmt != null) stmt.close();//Cerramos el Statement 
        } 
    }
}
