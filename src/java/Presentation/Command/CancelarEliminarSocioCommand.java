/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.SocioBLL;
import Entidad.Socio;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase permiten al usuario cancelar la eliminación de un socio
 * @author juan
 */
public class CancelarEliminarSocioCommand extends ICommand{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return "/mostrarSocios.jsp";
    }
    
}
