/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;
import Entidad.Utilidad.Log;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
/**
 * Los objetos de esta clase permiten definir y cerrar conexiones 
 * con la base de datos
 * @author juan
 */
public class Conexion_DB {
    /**
     * Intenta definir una conexión con la base de datos proyectodb
     * @return El objeto Connection definido 
     * @throws Exception si no se logra definir la conexión
     */
    public Connection AbrirConexion() throws Exception 
    {
        Connection con=null;  // Instancia una conexión
        try {
           Class.forName("com.mysql.jdbc.Driver");  // Cargar el driver
           String urlOdbc = "jdbc:mysql://localhost:3306/proyectodb";
           con=(java.sql.DriverManager.getConnection(urlOdbc,"root",""));  //Crea conexión
           return con;
         } catch(Exception e){//SQLException y ClassNotFoundException
            Log.getInstance().error(e);
            throw new Exception("Ha sido imposible establecer la conexion"+e.getMessage());
         }          
    }
   /**
    * Cierra la conexión que recibe como parámetro
    * @param con Es la conexión que hay que cerrar
    * @throws Exception Es la excepción que se lanza si no se logra cerrar la conexión
    */
    
    public  void CerrarConexion(Connection con) throws Exception
    {
        try {
             if (con!= null) con.close();    
        } catch (SQLException e) {
            Log.getInstance().error(e);
            throw new Exception("Ha sido imposible cerrar la conexion"+e.getMessage());
        }    
        }    
}
