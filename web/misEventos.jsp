<%@page import="Entidad.Utilidad.Fechas"%>
<%@page import="Entidad.Socio"%>
<%@page import="java.util.List"%>
<%@page import="Entidad.Evento"%>
<% 
   Socio socioSesion = (Socio) request.getSession().getAttribute("SocioSesion");
   List<Evento> listaEventosPropuestos= socioSesion.getEventosPropuestos();
%>
<h1>Mis eventos</h1>
<ol>
    <%
        for (Evento evento : listaEventosPropuestos) {
            int codigo = evento.getCodigo();
            String titulo = evento.getTitulo();
            String descripcion = evento.getDescripcion();
            String tipo = evento.getTipo().toString();
            String fecha = Fechas.getFechaEsp(evento.getFecha());
            String hora = evento.getHora();
            String lugar = evento.getLugar();
            int nsocio = evento.getNsocio();
    %>
    <li>
       <span>C�digo:</span><%=codigo%>
       <span>Socio:</span><%=nsocio%>
       <span>T�tulo:</span><%=titulo%>
       <span>Tipo:</span><%=tipo%>
       <span>Fecha:</span><%=fecha%>
       <span>Hora:</span><%=hora%>
       <span>Lugar:</span><%=lugar%><br>
       <span>Descripci�n:</span><br><%=descripcion%>
       <p class="derecha"><a href="Controller?opID=MostrarModificarEvento&codigoEvento=<%=codigo%>">Modificar</a>
       <a href="Controller?opID=ConfirmarEliminarEvento&codigoEvento=<%=codigo%>">Eliminar</a></p>
    </li>
        
    <%
        }
    %>
</ol>

