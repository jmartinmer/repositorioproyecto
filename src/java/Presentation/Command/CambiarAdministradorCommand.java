/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.SocioBLL;
import Entidad.Socio;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se usan para cambiar los socios administradores
 * @author juan
 */
public class CambiarAdministradorCommand extends ICommand{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int nsocio=Integer.parseInt(request.getParameter("nsocio"));
        Socio socio=new Socio();
        socio.setNsocio(nsocio);
        SocioBLL socioBLL= new SocioBLL();
        socio=socioBLL.findByNsocio(socio);
        socioBLL.cambiarAdministrador(socio);
        //Actualiza el listado de socios
        Socio socioSesion=(Socio) request.getSession().getAttribute("SocioSesion");
        List<Socio> listaSocios= socioBLL.obtenerSocios(socioSesion);
        request.setAttribute("listaSocios", listaSocios);        
        return "/mostrarSocios.jsp";
        
    }
    
}
