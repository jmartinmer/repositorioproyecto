<%@page import="java.util.List"%>
<%@page import="Entidad.Socio"%>
<% List<Socio> listaParticipantes = (List<Socio>) request.getAttribute("listaParticipantes");%>
<h1>Participantes</h1>
<table>
    <tr>
        <th>N� Socio</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Email</th>
    </tr>
    <%for(Socio socio:listaParticipantes){
        int nsocio=socio.getNsocio();
        String nombre= socio.getNombre();
        String apellidos= socio.getApellidos();
        String email= socio.getEmail();
    %>
    <tr>
        <td><%=nsocio%></td>
        <td><%=nombre%></td>
        <td><%=apellidos%></td>
        <td><a href="mailto:<%=email%>"><%=email%></a></td>
    </tr>
    <%
    }
    %>
</table>
