/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.EventoBLL;
import Entidad.Evento;
import Entidad.Socio;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utlizan para mostrar la información de los eventos de un socio
 * @author juan
 */
public class MisEventosCommand extends ICommand{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        EventoBLL eventoBLL= new EventoBLL();
        Socio socioSesion=(Socio)request.getSession().getAttribute("SocioSesion");
        List<Evento> listaEventosPropuestos= eventoBLL.obtenerEventosPropuestos(socioSesion);
        socioSesion.setEventosPropuestos(listaEventosPropuestos);
        request.getSession().setAttribute("SocioSesion",socioSesion);
        if (listaEventosPropuestos==null ||listaEventosPropuestos.size()==0){
            request.setAttribute("mensaje", "Sin eventos");
            return "/mensaje.jsp";
        }
        return "/misEventos.jsp";
    }
    
}
