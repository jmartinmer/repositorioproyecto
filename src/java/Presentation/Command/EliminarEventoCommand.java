/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.EventoBLL;
import Entidad.Evento;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utliizan para eliminar un evento
 * @author juan
 */
public class EliminarEventoCommand extends ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Evento evento = (Evento) request.getSession().getAttribute("EventoSeleccionado");
        EventoBLL eventoBLL = new EventoBLL();
        eventoBLL.eliminarEvento(evento);
        request.setAttribute("mensaje", "Evento eliminado con éxito");
        return "/mensaje.jsp";
    }

}
