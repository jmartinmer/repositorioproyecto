/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.SocioBLL;
import Entidad.Socio;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utlizan para el login del socio
 * @author juan
 */
public class LoginCommand extends ICommand {

    @Override

    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Socio _socio = new Socio();//Crea un objeto Cliente los datos ingresados
        _socio.setDni(request.getParameter("dni"));
        _socio.setPassword(request.getParameter("password"));

        SocioBLL _socioBLL = new SocioBLL();
       _socio = _socioBLL.validaSocio(_socio);
        request.getSession().setAttribute("SocioSesion", _socio);
        return null;
    }
}
