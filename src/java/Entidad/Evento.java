/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

import java.util.List;

/**
 * Un objeto de esta clase representa un evento propuesto por un socio
 * @author juan
 */
public class Evento {
    private int codigo;
    private String titulo;
    private String descripcion;
    private TipoEvento tipo;
    private String fecha;
    private String hora;
    private String lugar;
    /**
     * Número de socio que propuso el evento 
     */
    private int nsocio;
    private List<Socio> participantes;

    public List<Socio> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(List<Socio> participantes) {
        this.participantes = participantes;
    }
    
    

    public int getNsocio() {
        return nsocio;
    }

    public void setNsocio(int nsocio) {
        this.nsocio = nsocio;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoEvento getTipo() {
        return tipo;
    }

    public void setTipo(TipoEvento tipo) {
        this.tipo = tipo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }
    
}
