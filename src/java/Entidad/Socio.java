/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

import java.util.List;

/**
 * Un objeto de esta clase representa a un socio
 * @author juan
 */
public class Socio {
    /**
     * Número de socio
     */
    private int nsocio;
    private String nombre;
    private String apellidos;
    private String fechnacimiento;
    private String dni;
    private String ciudad;
    private String pais;
    private String telefono;
    private String email;
    private boolean administrador;
    private String password;
    private List<Evento> eventosPropuestos;
    private List<Evento> eventosParticipa;

    public int getNsocio() {
        return nsocio;
    }

    public void setNsocio(int nsocio) {
        this.nsocio = nsocio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechnacimiento() {
        return fechnacimiento;
    }

    public void setFechnacimiento(String fechnacimiento) {
        this.fechnacimiento = fechnacimiento;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Evento> getEventosPropuestos() {
        return eventosPropuestos;
    }

    public void setEventosPropuestos(List<Evento> eventosPropuestos) {
        this.eventosPropuestos = eventosPropuestos;
    }

    public List<Evento> getEventosParticipa() {
        return eventosParticipa;
    }

    public void setEventosParticipa(List<Evento> eventosParticipa) {
        this.eventosParticipa = eventosParticipa;
    }
    
    
}
