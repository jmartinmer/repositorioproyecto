/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.EventoBLL;
import Entidad.Evento;
import Entidad.Socio;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utlizan para mostrar los participantes a un evento
 * @author juan
 */
public class MostrarParticipantesCommand extends ICommand{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
       //recogemos el  evento
        int codigoEvento = Integer.parseInt(request.getParameter("codigoEvento"));
        Evento evento=new Evento();
        evento.setCodigo(codigoEvento);
        //ahora hay que obtener la lista de socios que participan en el evento 
        EventoBLL eventoBLL= new EventoBLL();
        List<Socio> listaParticipantes= eventoBLL.obtenerParticipantes(evento);
        request.setAttribute("listaParticipantes", listaParticipantes);
        return "/mostrarParticipantes.jsp";
        
    }
    
}
