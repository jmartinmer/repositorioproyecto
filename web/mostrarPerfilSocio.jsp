<%@page import="Entidad.Socio"%>
<%Socio _socioSesion = (Socio) request.getSession().getAttribute("SocioSesion");
    String nsocio = Integer.toString(_socioSesion.getNsocio());
    String dni = _socioSesion.getDni();
    String nombre = _socioSesion.getNombre();
    String apellidos = _socioSesion.getApellidos();
    String fechnacimiento = _socioSesion.getFechnacimiento();
    String ciudad = _socioSesion.getCiudad();
    String pais = _socioSesion.getPais();
    String telefono = _socioSesion.getTelefono();
    String email = _socioSesion.getEmail();
    String password = _socioSesion.getPassword();
    boolean administrador = _socioSesion.isAdministrador();
%>

<h1>Editar perfil socio</h1>
<form name ="form" action="Controller?opID=EditarPerfilSocio" method="POST">
    <p><label>N� Socio</label><input type="text"  name="nsocio" readonly value=<%=nsocio%>></p>
    <p><label>DNI</label><input type="text"  name="dni" readonly value=<%=dni%>></p>
    <p><label>Nombre</label><input type="text"  name="nombre" maxlength="20" required value=<%=nombre%>></p>
    <p><label>Apellidos</label><input type="text"  name="apellidos" maxlength="40" required value=<%=apellidos%>></p>
    <p><label>Fecha de nacimiento</label><input type="date"  name="fechnacimiento" required value=<%=fechnacimiento%>></p>
    <p><label>Ciudad</label><input type="text"  name="ciudad" maxlength="30" required value=<%=ciudad%>></p>
    <p><label>Pais</label><input type="text"  name="pais" maxlength="30" required value=<%=pais%>></p>
    <p><label>Tel�fono</label><input type="text"  name="telefono" pattern="\d{9,12}" maxlength="12" title="Entre 9 y 12 cifras" required value=<%=telefono%>></p>
    <p><label>Email</label><input type="email"  name="email" maxlength="30" required value=<%=email%>></p>
    <p><label>Contrase�a</label><input type="text"  name="password" maxlength="20" required value=<%=password%>></p>
    <p><input type="text"  name="administrador" hidden value=<%=administrador%>></p>
    <p><button>Enviar</button></p>
</form>