<%@page import="java.util.List"%>
<%@page import="Entidad.Socio"%>
<% List<Socio> listaSocios = (List<Socio>) request.getAttribute("listaSocios");%>
<h1>Listado de socios</h1>
<table>
    <tr>
        <th>N� Socio</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Email</th>
        <th>Administrador</th>
    </tr>
    <%for (Socio socio : listaSocios) {
            int nsocio = socio.getNsocio();
            String nombre = socio.getNombre();
            String apellidos = socio.getApellidos();
            String email = socio.getEmail();
            boolean administrador = socio.isAdministrador();
       // String admin= administrador? "S�":"NO";
%>
    <tr>
        <td><%=nsocio%></td>
        <td><%=nombre%></td>
        <td><%=apellidos%></td>
        <td><a href="mailto:<%=email%>"><%=email%></a></td>
        <td><input type="checkbox" onchange="window.location.assign('Controller?opID=CambiarAdministrador&nsocio='+<%=nsocio%>)"
                   <%
                       if (administrador) {%>
                   checked
                   <% }%>
                   ></td>
        <td class="sinborde"><a href="Controller?opID=ConfirmarEliminarSocio&nsocio=<%=nsocio%>">Eliminar</a></td>
    </tr>
    <%
        }
    %>
</table>
