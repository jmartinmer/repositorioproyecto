/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.SocioBLL;
import Entidad.Socio;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase permiten al usuario confirmar la eliminación de un socio
 * @author juan
 */
public class ConfirmarEliminarSocioCommand extends ICommand{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int nsocio=Integer.parseInt(request.getParameter("nsocio"));
        Socio socioSeleccionado=new Socio();
        socioSeleccionado.setNsocio(nsocio);
        SocioBLL socioBLL= new SocioBLL();
        socioSeleccionado= socioBLL.findByNsocio(socioSeleccionado);
        request.getSession().setAttribute("SocioSeleccionado", socioSeleccionado);
        return "/confirmarEliminarSocio.jsp";
    }
    
}
