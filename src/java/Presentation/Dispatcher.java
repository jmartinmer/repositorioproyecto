package Presentation;

import Entidad.Exceptions.ProgException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Un objeto de esta clase sirve para que la aplicación navegue a la página correspondiente
 * a la petición hecha por el usuario
 * @author juan
 */
public class Dispatcher {
    /**
     * Este método establece el valor adecuado del atributo de sesión "paginaprincipal" 
     * que representa la siguietne página a cargar
     * @param request
     * @param response
     * @param nextPage
     * @throws ServletException
     * @throws IOException 
     */
    public void procesa(HttpServletRequest request, HttpServletResponse response,String nextPage)
    throws ServletException, IOException {
        String _beforePage="";
        try
        {
            response.setContentType("text/html;charset=UTF-8");
            request.setAttribute("paginaprincipal", nextPage);
            _beforePage=(String) request.getSession().getAttribute("paginaprincipal");
            request.getSession().setAttribute("paginaprincipal", nextPage);
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }catch(IOException ex)
        {
        request.getSession().setAttribute("paginaprincipal", _beforePage);
        throw ex;
        }catch(ServletException ex)
        {
        request.getSession().setAttribute("paginaprincipal", _beforePage);
        throw ex;
        }
    } 
    /**
     * A este método se invoca cuando se produce una excepción del tipo ProgException
     * da valor adecuado al atributo de petición "advertencia" que hará que se visualice
     * en la siguiente página cargada un mensaje de error programado
     * @param request
     * @param response
     * @param ex
     * @throws ServletException
     * @throws IOException 
     */
    public void procesa(HttpServletRequest request, HttpServletResponse response, ProgException ex) 
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String _beforePage=(String) request.getSession().getAttribute("paginaprincipal");
        request.setAttribute("paginaprincipal", _beforePage);
        request.setAttribute("advertencia", ex.getMessageError());
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
