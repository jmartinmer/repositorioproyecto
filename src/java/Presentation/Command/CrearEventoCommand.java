/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.EventoBLL;
import Entidad.Evento;
import Entidad.TipoEvento;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utilizan para crear un nuevo evento
 * @author juan
 */
public class CrearEventoCommand extends ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html;charset=UTF-8");

        String titulo = request.getParameter("titulo");
        String descripcion = request.getParameter("descripcion");
        TipoEvento tipo = TipoEvento.valueOf(request.getParameter("tipo"));
        String fecha = request.getParameter("fecha");
        String hora = request.getParameter("hora");
        String lugar = request.getParameter("lugar");
        int nsocio = Integer.parseInt(request.getParameter("nsocio"));

        Evento evento = new Evento();
        evento.setTitulo(titulo);
        evento.setDescripcion(descripcion);
        evento.setTipo(tipo);
        evento.setFecha(fecha);
        evento.setHora(hora);
        evento.setLugar(lugar);
        evento.setNsocio(nsocio);
        EventoBLL eventoBLL = new EventoBLL();
        eventoBLL.crearEvento(evento);
        request.setAttribute("mensaje", "Evento creado con éxito");

        return "/mensaje.jsp";
    }

}
