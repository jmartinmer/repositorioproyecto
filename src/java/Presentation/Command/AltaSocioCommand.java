/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.SocioBLL;
import Entidad.Exceptions.ProgException;
import Entidad.Socio;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Los objetos de esta clase se utilizan para registrar un nuevo socio
 * @author juan
 */
public class AltaSocioCommand extends ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        String dni = request.getParameter("dni");
        String nombre = request.getParameter("nombre");
        String apellidos = request.getParameter("apellidos");
        String fechnacimiento = request.getParameter("fechnacimiento");
        String ciudad = request.getParameter("ciudad");
        String pais = request.getParameter("pais");
        String telefono = request.getParameter("telefono");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        boolean administrador = false;
        Socio socio = new Socio();
        socio.setDni(dni);
        socio.setNombre(nombre);
        socio.setApellidos(apellidos);
        socio.setFechnacimiento(fechnacimiento);
        socio.setCiudad(ciudad);
        socio.setPais(pais);
        socio.setTelefono(telefono);
        socio.setEmail(email);
        socio.setPassword(password);
        socio.setAdministrador(administrador);
        SocioBLL socioBLL = new SocioBLL();
        socioBLL.altaSocio(socio);
        request.setAttribute("mensaje", "Registrado con éxito");
        return "/mensaje.jsp";

    }

}
