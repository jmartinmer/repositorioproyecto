/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entidad.Socio;
import Entidad.Utilidad.Log;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Los objetos de esta clase sirven para hacer consultas a la base de datos
 * sobre socios
 *
 * @author juan
 */
public class SocioDAO {
/**
 * Este método da valor a todos los campos del socio que recibe como parámetro, salvo el número de socio
 * @param rs La fila del resultSet que contiene la información que hay que darle al socio
 * @param socio
 * @throws SQLException 
 */
    public  static void obtenSocioFila(ResultSet rs, Socio socio) throws SQLException {
        socio.setDni(rs.getString("dni"));
        socio.setNombre(rs.getString("nombre"));
        socio.setApellidos(rs.getString("apellidos"));
        socio.setFechnacimiento(rs.getString("fechnacimiento"));
        socio.setCiudad(rs.getString("ciudad"));
        socio.setPais(rs.getString("pais"));
        socio.setTelefono(rs.getString("telefono"));
        socio.setEmail(rs.getString("email"));
        socio.setPassword(rs.getString("password"));
        socio.setAdministrador(rs.getBoolean("administrador"));
        socio.setNsocio(rs.getInt("nsocio"));
    }
/**
 * Consulta en la base de datos para obtener el registro del socio cuyo dni es el del objeto socio 
 * que recibe el método como parámetro
 * @param con
 * @param socio
 * @return Un objeto Socio que contiene información en todos sus campos
 * @throws Exception 
 */
    public Socio findByDNI(Connection con, Socio socio) throws Exception {
        Socio _socio = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM socios WHERE dni=?");
            stmt.setString(1, socio.getDni());
            rs = stmt.executeQuery();
            while (rs.next()) {
                _socio = new Socio();
                obtenSocioFila(rs, _socio);
            }
        } catch (SQLException ex) {
            Log.getInstance().error(ex);
            throw new Exception("Ha habido un problema al buscar el socio por DNI " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close(); //Cerramos el resulset
            }
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
        return _socio;
    }
 /**
  * Inserta un nuevo registro en la tabla de socios
  * @param con
  * @param socio
  * @throws Exception 
  */   
     public  void altaSocio(Connection con, Socio socio) throws Exception {
        PreparedStatement stmt=null; 
        try {
            //los nombres de los campos no van entre ' (si podrían ir entre ´)
            stmt = con.prepareStatement("INSERT INTO socios(nombre,apellidos,"
                    + "fechnacimiento,dni,ciudad,pais,telefono,email,"
                    + "administrador,password) VALUES(?,?,?,?,?,?,?,?,?,?)");
            stmt.setString(1,socio.getNombre());
            stmt.setString(2,socio.getApellidos());
            stmt.setString(3,socio.getFechnacimiento());
            stmt.setString(4,socio.getDni());
            stmt.setString(5,socio.getCiudad());
            stmt.setString(6,socio.getPais());
            stmt.setString(7,socio.getTelefono());
            stmt.setString(8,socio.getEmail());
            stmt.setBoolean(9,socio.isAdministrador());
            stmt.setString(10,socio.getPassword());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
           throw new Exception("Ha habido un problema al registrar el socio "+ex.getMessage());
            
        }  finally
        {
            if (stmt != null) stmt.close();//Cerramos el Statement 
        }   
    } 
   /**
 * Consulta en la base de datos para obtener el registro del socio cuyo número de socio es el del objeto socio 
 * que recibe el método como parámetro
 * @param con
 * @param socio
 * @return Un objeto Socio que contiene información en todos sus campos
 * @throws Exception 
 */ 
     public Socio findByNsocio(Connection con, Socio socio) throws Exception {
        Socio _socio = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM socios WHERE nsocio=?");
            stmt.setInt(1, socio.getNsocio());
            rs = stmt.executeQuery();
            while (rs.next()) {
                _socio = new Socio();
                obtenSocioFila(rs, _socio);
            }
        } catch (SQLException ex) {
            Log.getInstance().error(ex);
            throw new Exception("Ha habido un problema al buscar el socio por número de socio" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close(); //Cerramos el resulset
            }
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
        return _socio;
    }
    /**
     * Modifica la información del socio en la tabla de socios
     * @param con
     * @param socio Contiene la nueva información del socio
     * @throws Exception 
     */
     public  void editarSocio(Connection con, Socio socio) throws Exception {
        PreparedStatement stmt=null; 
        try {
            stmt = con.prepareStatement("UPDATE socios SET nombre=?,apellidos=?,fechnacimiento=?,dni=?,ciudad=?,pais=?,telefono=?,email=?,administrador=?,password=? WHERE nsocio=?");
            stmt.setString(1,socio.getNombre());
            stmt.setString(2,socio.getApellidos());
            stmt.setString(3,socio.getFechnacimiento());
            stmt.setString(4,socio.getDni());
            stmt.setString(5,socio.getCiudad());
            stmt.setString(6,socio.getPais());
            stmt.setString(7,socio.getTelefono());
            stmt.setString(8,socio.getEmail());
            stmt.setBoolean(9,socio.isAdministrador());
            stmt.setString(10,socio.getPassword());
            stmt.setInt(11,socio.getNsocio());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Ha habido un problema al editar el socio "+ex.getMessage());
        }  finally
        {
            if (stmt != null) stmt.close();//Cerramos el Statement 
        }   
    }
/**
 * Devuelve la lista de todos los socios salvo el socio en sesión
 * @param con
 * @param socio
 * @return
 * @throws Exception 
 */
    public List<Socio> obtenerSocios(Connection con, Socio socio) throws Exception {
        List<Socio> _listaSocios = new ArrayList();
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM socios WHERE nsocio!=?");
            stmt.setInt(1, socio.getNsocio());
            rs = stmt.executeQuery();
            Socio _socio = null;
            while (rs.next()) {
                _socio = new Socio();
                SocioDAO.obtenSocioFila(rs, _socio);
                _listaSocios.add(_socio);
            }
        } catch (SQLException ex) {
            Log.getInstance().error(ex);
            throw new Exception("Ha habido un problema al buscar los socios" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close(); //Cerramos el resulset
            }
            if (stmt != null) {
                stmt.close();//Cerramos el Statement 
            }
        }
        return _listaSocios;
    }
/**
 * Elimina el socio de la tabla de socios
 * @param con
 * @param socio
 * @throws Exception 
 */
    public void eliminarSocio(Connection con, Socio socio) throws Exception {
              PreparedStatement stmt=null; 
        try {
            stmt = con.prepareStatement("DELETE FROM socios WHERE nsocio=?");
            stmt.setInt(1,socio.getNsocio());
            stmt.executeUpdate();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Ha habido un problema al eliminar el socio "+ex.getMessage());
        }  finally
        {
            if (stmt != null) stmt.close();//Cerramos el Statement 
        } 
    }
    /**
     * Modifica el valor del campo boolean administrador del socio que recibe como parámetro
     * @param con
     * @param socio
     * @throws Exception 
     */
    public  void cambiarAdministrador(Connection con, Socio socio) throws Exception {
        PreparedStatement stmt=null; 
        try {
            stmt = con.prepareStatement("UPDATE socios SET administrador=? WHERE nsocio=?");
            //el nuevo valor del campo administrador tiene que ser lo contrario del valor que tenga ahora
            stmt.setBoolean(1,!socio.isAdministrador());
            stmt.setInt(2,socio.getNsocio());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Ha habido un problema al cambiar administrador "+ex.getMessage());
        }  finally
        {
            if (stmt != null) stmt.close();//Cerramos el Statement 
        }   
    }
}
