<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Entidad.Socio"%>
<!DOCTYPE html>
<html>
    <head>
        <LINK REL=StyleSheet HREF="estilos\general.css" TYPE="text/css">
        <!--<meta http-equiv="Content-Type" charset="UTF-8">-->
        <meta charset="utf-8" />
        <title>Inicio</title>
    </head>
    <body>
        <header>
            <img src="imagenes\fans.jpg" alt="Fans">
        </header>
        
         <jsp:include page="menu.jsp" />
         <main>
            <%String paginaprincipal = (String) request.getAttribute("paginaprincipal");
                 if (paginaprincipal != null) {%> 
            <jsp:include page='<%=(String) request.getAttribute("paginaprincipal")%>' />
            <%}
                 else{%>
                      <img src="imagenes\gol.jpg" alt="Gol" id="gol">
                 <% }
            %>  
            <%String _advertencia = (String) request.getAttribute("advertencia");%>
             <p class="advert"><% if (_advertencia != null) {%><%=_advertencia%><%}%> </p> 
            
        </main>
        <footer>
            <p>2020 - Creado por Juan Martín Merino</p>
            <p>I.E.S. Juan de Garay (Valencia)</p>
        </footer>
    </body>
</html>
