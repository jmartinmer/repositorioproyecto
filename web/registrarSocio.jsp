<h1>Registro nuevo socio</h1>
<form name ="form" action="Controller?opID=AltaSocio" method="POST" >
    <p><label>DNI (sin letra)</label><input type="text"  name="dni" pattern="\d{7,8}" maxlength="8" title="7 u 8 cifras sin letra" required></p>
    <p><label>Nombre</label><input type="text"  name="nombre" maxlength="20" required></p>
    <p><label>Apellidos</label><input type="text"  name="apellidos" maxlength="40" required></p>
    <p><label>Fecha de nacimiento</label><input type="date"  name="fechnacimiento" required></p>
    <p><label>Ciudad</label><input type="text"  name="ciudad" maxlength="30" required></p>
    <p><label>Pais</label><input type="text"  name="pais" maxlength="30" required></p>
    <p><label>Tel�fono</label><input type="text"  name="telefono" pattern="\d{9,12}" maxlength="12" title="Entre 9 y 12 cifras" required></p>
    <p><label>Email</label><input type="email"  name="email" maxlength="30" required></p>
    <p><label>Contrase�a</label><input type="password"  name="password" maxlength="20" required></p>
    <p><button>Enviar</button></p>
</form>