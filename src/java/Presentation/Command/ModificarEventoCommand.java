/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.EventoBLL;
import Entidad.Evento;
import Entidad.TipoEvento;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utlizan para modificar la información de un evento
 * @author juan
 */
public class ModificarEventoCommand extends ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //Definimos el mime-type de la respuesta
        response.setContentType("text/html;charset=UTF-8");
        String titulo = request.getParameter("titulo");
        String tipo = request.getParameter("tipo");
        String fecha = request.getParameter("fecha");
        String hora = request.getParameter("hora");
        String lugar = request.getParameter("lugar");
        String descripcion = request.getParameter("descripcion");
        Evento evento = (Evento) request.getSession().getAttribute("EventoSeleccionado");
        evento.setTitulo(titulo);
        evento.setTipo(TipoEvento.valueOf(tipo));
        evento.setFecha(fecha);
        evento.setHora(hora);
        evento.setLugar(lugar);
        evento.setDescripcion(descripcion);
        EventoBLL eventoBLL = new EventoBLL();
        eventoBLL.modificarEvento(evento);
        request.getSession().setAttribute("EventoSeleccionado", evento);
        request.setAttribute("mensaje", "Evento modificado con éxito");

        return "/mensaje.jsp";
    }

}
