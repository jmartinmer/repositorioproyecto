/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Command;

import BLL.SocioBLL;
import Entidad.Socio;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Los objetos de esta clase se utilizan para eliminar un socio
 * @author juan
 */
public class EliminarSocioCommand extends ICommand{
/**
 * este método utiliza el atributo de sesión "SocioSeleccionado" para saber qué socio
 * es el que el usuario seleccionó eliminar
 * Es un atributo de sesión y no de petición porque entre la solicitud de eliminación
 * y la eliminación hay una página de confirmación
 * @param request
 * @param response
 * @return
 * @throws Exception 
 */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Socio socio = (Socio) request.getSession().getAttribute("SocioSeleccionado");
        SocioBLL socioBLL = new SocioBLL();
        socioBLL.eliminarSocio(socio);
        request.setAttribute("mensaje", "Socio eliminado con éxito");
        return "/mensaje.jsp";
    }
    
}
